﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace Lails.Server
{
    public static partial class ControllerExtensions
    {
        public static string GetAlias(this ApiController controller, string key = "aliasname")
        {
            if (string.IsNullOrWhiteSpace(key))
            {
                key = "aliasname";
            }

            if (controller.Request != null && controller.Request.Properties != null && controller.Request.Properties.ContainsKey(key))
            {
                return controller.Request.Properties[key]?.ToString();
            }
            return null;
        }
    }
}
