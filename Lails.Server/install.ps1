﻿param($installPath, $toolsPath, $package, $project)

# NLog.config
$configItem = $project.ProjectItems.Item("NLog.config")
$copyToOutput = $configItem.Properties.Item("CopyToOutputDirectory")
$copyToOutput.Value = 1
$buildAction = $configItem.Properties.Item("BuildAction")
$buildAction.Value = 2

# Config\config.json
$configItem = $project.ProjectItems.Item("Config").ProjectItems.Item("config.json");
$copyToOutput = $configItem.Properties.Item("CopyToOutputDirectory")
$copyToOutput.Value = 1
$buildAction = $configItem.Properties.Item("BuildAction")
$buildAction.Value = 2

# Shell\Install.bat
$configItem = $project.ProjectItems.Item("Shell").ProjectItems.Item("Install.bat");
$copyToOutput = $configItem.Properties.Item("CopyToOutputDirectory")
$copyToOutput.Value = 1
$buildAction = $configItem.Properties.Item("BuildAction")
$buildAction.Value = 2

# Shell\UnInstall.bat
$configItem = $project.ProjectItems.Item("Shell").ProjectItems.Item("UnInstall.bat");
$copyToOutput = $configItem.Properties.Item("CopyToOutputDirectory")
$copyToOutput.Value = 1
$buildAction = $configItem.Properties.Item("BuildAction")
$buildAction.Value = 2

# Shell\Start.bat
$configItem = $project.ProjectItems.Item("Shell").ProjectItems.Item("Start.bat");
$copyToOutput = $configItem.Properties.Item("CopyToOutputDirectory")
$copyToOutput.Value = 1
$buildAction = $configItem.Properties.Item("BuildAction")
$buildAction.Value = 2

# Shell\Stop.bat
$configItem = $project.ProjectItems.Item("Shell").ProjectItems.Item("Stop.bat");
$copyToOutput = $configItem.Properties.Item("CopyToOutputDirectory")
$copyToOutput.Value = 1
$buildAction = $configItem.Properties.Item("BuildAction")
$buildAction.Value = 2